# BPSK误码率曲线MATLAB代码

## 简介

本仓库提供了一个用于生成BPSK（二进制相移键控）误码率曲线的MATLAB代码。通过运行该代码，您可以生成并绘制BPSK信号在不同信噪比（SNR）条件下的误码率曲线。

## 文件说明

- `BPSK_BER_Curve.m`: 主MATLAB脚本，用于生成BPSK误码率曲线。

## 使用方法

1. 下载或克隆本仓库到您的本地环境。
2. 打开MATLAB软件。
3. 在MATLAB中打开`BPSK_BER_Curve.m`文件。
4. 运行脚本，生成并显示BPSK误码率曲线。

## 依赖项

- MATLAB R2016b或更高版本。

## 贡献

欢迎提交问题、建议或改进代码的拉取请求。

## 许可证

本项目采用MIT许可证。有关更多信息，请参阅[LICENSE](LICENSE)文件。